import React from "react";
import { shallow } from "enzyme";
import "jest-styled-components";

import Button from ".";

describe("Button", () => {
  it("renders the text passed in", () => {
    let testObject = shallow(<Button>Hello</Button>);
    expect(testObject.children().text()).toContain("Hello");
  });
});
