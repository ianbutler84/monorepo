import React from "react";
import "jest-styled-components";
import { shallow } from "enzyme";

import AltButton from ".";

describe("AltButton", () => {
  it("renders the text passed in", () => {
    let testObject = shallow(<AltButton>Hello</AltButton>);
    expect(testObject.children().text()).toContain("Hello");
  });
});
