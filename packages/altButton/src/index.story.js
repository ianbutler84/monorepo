import React from "react";
import { storiesOf } from "@storybook/react";

import AltButton from ".";

storiesOf("AltButton", module).add("default", () => (
  <AltButton>{"AltButton"}</AltButton>
));
