module.exports = {
  cacheDirectory: ".jest-cache",
  // Automatically clear mock calls and instances between every test
  clearMocks: true,
  coverageDirectory: ".jest-coverage",
  coveragePathIgnorePatterns: ["<rootDir>/packages/(?:.+?)/lib/"],
  coverageReporters: ["html", "text"],
  coverageThreshold: {
    global: {
      branches: 100,
      functions: 100,
      lines: 100,
      statements: 100
    }
  },
  // An array of file extensions your modules use
  moduleFileExtensions: ["js", "json", "jsx"],
  // The paths to modules that run some code to configure or set up the testing environment before each test
  setupFiles: ["<rootDir>/enzyme.config.js"],
  // The test environment that will be used for testing
  testEnvironment: "jsdom",
  // The glob patterns Jest uses to detect test files
  testMatch: ["**/__tests__/**/*.js?(x)", "**/?(*.)+(spec|test).js?(x)"],
  testPathIgnorePatterns: ["<rootDir>/packages/(?:.+?)/lib/"]
};
